# translation of katebuild-plugin.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2008, 2009, 2010, 2011.
# Sönke Dibbern <s_dibbern@web.de>, 2009, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2014-08-14 16:55+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Manfred Wiese, Sönke Dibbern"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "m.j.wiese@web.de, s_dibbern@web.de"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Utgaav"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Nochmaak buen"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Afbreken"

#: plugin_katebuild.cpp:149 plugin_katebuild.cpp:156 plugin_katebuild.cpp:1125
#, kde-format
msgid "Build"
msgstr "Buen"

#: plugin_katebuild.cpp:159
#, fuzzy, kde-format
#| msgid "Build Target..."
msgid "Select Target..."
msgstr "Teel buen..."

#: plugin_katebuild.cpp:164
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build Selected Target"
msgstr "Utsöcht Teel buen"

#: plugin_katebuild.cpp:169
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and Run Selected Target"
msgstr "Utsöcht Teel buen"

#: plugin_katebuild.cpp:174
#, kde-format
msgid "Stop"
msgstr "Anhollen"

#: plugin_katebuild.cpp:179
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:199
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:221
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Teel-Instellen"

#: plugin_katebuild.cpp:340
#, fuzzy, kde-format
#| msgid "Build command:"
msgid "Build Information"
msgstr "Buubefehl:"

#: plugin_katebuild.cpp:421 plugin_katebuild.cpp:1178 plugin_katebuild.cpp:1189
#: plugin_katebuild.cpp:1210 plugin_katebuild.cpp:1220
#, kde-format
msgid "Project Plugin Targets"
msgstr "Projektmoduul-Telen"

#: plugin_katebuild.cpp:518
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "För't Buen is keen Datei oder Orner angeven."

#: plugin_katebuild.cpp:522
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "\"%1\" is keen lokaal Datei. Feern Dateien laat sik nich kompileren."

#: plugin_katebuild.cpp:569
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:583
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "\"%1\" lett sik nich utföhren. Utstiegstatus: %2"

#: plugin_katebuild.cpp:598
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Buen vun <b>%1</b> afbraken"

#: plugin_katebuild.cpp:705
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:719
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "För't Buen is keen lokaal Datei oder Orner angeven."

#: plugin_katebuild.cpp:725
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:752
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Teel <b>%1</b> warrt buut ..."

#: plugin_katebuild.cpp:766
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:807
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:813
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Een Fehler funnen"
msgstr[1] "%1 Fehlers funnen"

#: plugin_katebuild.cpp:817
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Een Wohrschoen funnen"
msgstr[1] "%1 Wohrschoen funnen"

#: plugin_katebuild.cpp:820
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Een Fehler funnen"
msgstr[1] "%1 Fehlers funnen"

#: plugin_katebuild.cpp:825
#, kde-format
msgid "Build failed."
msgstr "Buen fehlslaan"

#: plugin_katebuild.cpp:827
#, kde-format
msgid "Build completed without problems."
msgstr "Buen ahn Problemen afslaten."

#: plugin_katebuild.cpp:832
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:856
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1082
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "Fehler"

#: plugin_katebuild.cpp:1085
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "Wohrschoen"

#: plugin_katebuild.cpp:1088
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1091
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "undefined reference"

#: plugin_katebuild.cpp:1124
#, fuzzy, kde-format
#| msgid "Target set"
msgid "Target Set"
msgstr "Teelsett"

#: plugin_katebuild.cpp:1126
#, kde-format
msgid "Clean"
msgstr "Oprümen"

#: plugin_katebuild.cpp:1127
#, kde-format
msgid "Config"
msgstr "Instellen"

#: plugin_katebuild.cpp:1128
#, fuzzy, kde-format
#| msgid "Config"
msgid "ConfigClean"
msgstr "Instellen"

#: plugin_katebuild.cpp:1248
#, kde-format
msgid "build"
msgstr "Buen"

#: plugin_katebuild.cpp:1251
#, fuzzy, kde-format
#| msgid "Clean"
msgid "clean"
msgstr "Oprümen"

#: plugin_katebuild.cpp:1254
#, kde-format
msgid "quick"
msgstr ""

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:98
#, fuzzy, kde-format
#| msgid "Leave empty to use the directory of the current document. "
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr "Leddig laten un den Orner un dat aktuelle Dokment bruken. "

#: TargetHtmlDelegate.cpp:102
#, fuzzy, kde-format
#| msgid ""
#| "Use:\n"
#| "\"%f\" for current file\n"
#| "\"%d\" for directory of current file"
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Bruuk:\n"
"\"%f\" för de aktuelle Datei\n"
"\"%d\" för de aktuelle Datei ehr Orner"

#: TargetModel.cpp:388
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:391
#, fuzzy, kde-format
#| msgid "Working directory"
msgid "Working Directory / Command"
msgstr "Arbeitorner"

#: TargetModel.cpp:394
#, fuzzy, kde-format
#| msgid "Command:"
msgid "Run Command"
msgstr "Befehl:"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Nieg Sett vun Telen opstellen"

#: targets.cpp:31
#, fuzzy, kde-format
#| msgid "Copy set of targets"
msgid "Copy command or target set"
msgstr "Sett vun Telen koperen"

#: targets.cpp:35
#, fuzzy, kde-format
#| msgid "Delete current set of targets"
msgid "Delete current target or current set of targets"
msgstr "Aktuell Sett vun Telen wegdoon"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Nieg Teel tofögen"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Utsöcht Teel buen"

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and run selected target"
msgstr "Utsöcht Teel buen"

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target up"
msgstr "Utsöcht Teel buen"

#: targets.cpp:56
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target down"
msgstr "Utsöcht Teel buen"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Buen"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr ""

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr ""

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Buen vun <b>%1</b> afslaten."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Bi't Buen vun <b>%1</b> geev dat Fehlers."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Bi't Buen vun <b>%1</b> geev dat Wohrschoen."

#~ msgid "Show:"
#~ msgstr "Wiesen:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Datei"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Reeg"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Mellen"

#~ msgid "Next Error"
#~ msgstr "Nakamen Fehler"

#~ msgid "Previous Error"
#~ msgstr "Verleden Fehler"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "Fehler"

#, fuzzy
#~| msgid "Warnings"
#~ msgid "Warning"
#~ msgstr "Wohrschoen"

#~ msgid "Only Errors"
#~ msgstr "Bloots Fehlers"

#~ msgid "Errors and Warnings"
#~ msgstr "Fehlers un Wohrschoen"

#~ msgid "Parsed Output"
#~ msgstr "Dörkeken Utgaav"

#~ msgid "Full Output"
#~ msgstr "Heel Utgaav"

#, fuzzy
#~| msgid "Delete selected target"
#~ msgid "Select active target set"
#~ msgstr "Utsöcht Teel wegdoon"

#, fuzzy
#~| msgid "Build selected target"
#~ msgid "Filter targets"
#~ msgstr "Utsöcht Teel buen"

#~ msgid "Build Default Target"
#~ msgstr "Standardteel buen"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Standardteel buen"

#, fuzzy
#~| msgid "Build Previous Target Again"
#~ msgid "Build Previous Target"
#~ msgstr "Verleden Teel nochmaal buen"

#, fuzzy
#~| msgid "Config"
#~ msgid "config"
#~ msgstr "Instellen"

#~ msgid "Kate Build Plugin"
#~ msgstr "Kate-Buumoduul"

#, fuzzy
#~| msgid "Delete selected target"
#~ msgid "Select build target"
#~ msgstr "Utsöcht Teel wegdoon"

#~ msgid "Build Output"
#~ msgstr "Utgaav buen"

#~ msgid "Next Set of Targets"
#~ msgstr "Nakamen Teelsett"

#~ msgid "No previous target to build."
#~ msgstr "Keen verleden Teel, dat sik buen lett."

#~ msgid "No target set as default target."
#~ msgstr "Keen Teel as Standardteel fastleggt."

#~ msgid "No target set as clean target."
#~ msgstr "Keen Teel as Clean-Teel fastleggt."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Teel \"%1\" lett sik nich finnen."

#~ msgid "Really delete target %1?"
#~ msgstr "Teel %1 redig wegdoon?"

#~ msgid "Nothing built yet."
#~ msgstr "Betherto nix buut."

#~ msgid "Target Set %1"
#~ msgstr "Teelsett %1"

#~ msgid "Target"
#~ msgstr "Teel"

#~ msgid "Target:"
#~ msgstr "Teel:"

#~ msgid "from"
#~ msgstr "vun"

#~ msgid "Sets of Targets"
#~ msgstr "Teel-Setten"

#~ msgid "Make Results"
#~ msgstr "Resultaat vun \"make\""

#~ msgid "Others"
#~ msgstr "Anner"

#~ msgid "Quick Compile"
#~ msgstr "Fix kompileren"

#~ msgid "The custom command is empty."
#~ msgstr "De egen Befehl is leddig."

#~ msgid "New"
#~ msgstr "Nieg"

#~ msgid "Copy"
#~ msgstr "Koperen"

#~ msgid "Delete"
#~ msgstr "Wegmaken"

#~ msgid "Quick compile"
#~ msgstr "Fix kompileren"

#~ msgid "Run make"
#~ msgstr "\"Make\" opropen"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Break"
#~ msgstr "Aftrennen"

#~ msgid "There is no file to compile."
#~ msgstr "Dor is keen Datei to kompileren."
