# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2011, 2013, 2014, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-09 00:51+0000\n"
"PO-Revision-Date: 2023-04-14 23:06+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.03.90\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"Tu non ha satis kharma pro acceder a un shell o un emulation de terminal"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:652
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Pipe a terminal"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "S&ynchronisa terminal con documento currente"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Executa documento currente"

#: kateconsole.cpp:156 kateconsole.cpp:501
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Monstra pannello de &Terminal"

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Focalisa Pannello de Terminal"

#: kateconsole.cpp:301
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""
"Konsole non installate. Pro favor installa Konsole pro poter usar le "
"terminal."

#: kateconsole.cpp:376
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Tu vermente vole poner in pipe le texto al console? isto executara ulle "
"commandos continite con tu derectos de usator."

#: kateconsole.cpp:377
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Pipe a terminal?"

#: kateconsole.cpp:378
#, kde-format
msgid "Pipe to Terminal"
msgstr "Pipe a terminal"

#: kateconsole.cpp:406
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Desolate, non pote facer cd in '%1'"

#: kateconsole.cpp:442
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Non un file local:'%1'"

#: kateconsole.cpp:475
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Tu vermente vole executar le documento?\n"
"Isto executara le sequente commando,\n"
"con tu derectos de usator, in le terminal:\n"
"'%1'"

#: kateconsole.cpp:482
#, kde-format
msgid "Run in Terminal?"
msgstr "Executa in Terminal?"

#: kateconsole.cpp:483
#, kde-format
msgid "Run"
msgstr "Exeque"

#: kateconsole.cpp:498
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Cela Pannello de Terminal"

#: kateconsole.cpp:509
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "De-focalisa pannello de Terminal"

#: kateconsole.cpp:510 kateconsole.cpp:511
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Focalisa Pannello de Terminal"

#: kateconsole.cpp:585
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"Synchronisa &automaticamente le terminal con le documento currente quando il "
"es possibile"

#: kateconsole.cpp:589 kateconsole.cpp:610
#, kde-format
msgid "Run in terminal"
msgstr "Exeque in Terminal"

#: kateconsole.cpp:591
#, kde-format
msgid "&Remove extension"
msgstr "&Remove extension"

#: kateconsole.cpp:596
#, kde-format
msgid "Prefix:"
msgstr "Prefixo:"

#: kateconsole.cpp:604
#, kde-format
msgid "&Show warning next time"
msgstr "Mon&stra aviso proxime vice"

#: kateconsole.cpp:606
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"Le proxime vice que '%1' es executate, tu assecurate que il habera un "
"fenestra de aviso como popup, monstrante le commando que il debe esser "
"inviate al terminal, per revision."

#: kateconsole.cpp:617
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Fixa variabile de ambiente de &EDITOR a 'kate -b'"

#: kateconsole.cpp:620
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Importante: le documento debe esser claudite pro facer que le application de "
"console continua"

#: kateconsole.cpp:623
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Cela Konsole qundo on pressa 'Esc'"

#: kateconsole.cpp:626
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Isto pote causar problemas con appe de terminal que usa clave Esc, p.ex. "
"vim. Adde iste apps in le entrta a basso (lista separate per comma)"

#: kateconsole.cpp:657
#, kde-format
msgid "Terminal Settings"
msgstr "Preferentias de terminal"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "Ins&trumentos"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "Kate Terminal"
#~ msgstr "Terminal de Kate"

#~ msgid "Terminal Panel"
#~ msgstr "Pannello de Terminal"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgid "Embedded Konsole"
#~ msgstr "Konsole insertate"
