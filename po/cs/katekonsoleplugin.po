# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vit Pelcak <vit@pelcak.org>, 2012, 2013, 2014, 2018, 2020, 2021, 2023.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-09 00:51+0000\n"
"PO-Revision-Date: 2023-04-05 11:33+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.3\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"Vaše karma není dostatečně silná pro přístup k příkazovému procesoru nebo "
"terminálu"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:652
#, kde-format
msgid "Terminal"
msgstr "Terminál"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Poslat rourou na terminál"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "S&ynchronizovat terminál se současným dokumentem"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Spustit současný dokument"

#: kateconsole.cpp:156 kateconsole.cpp:501
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Zobrazit panel &terminálu"

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "Přepnout na panel &terminálu"

#: kateconsole.cpp:301
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""
"Konsole není nainstalována. Prosím, nainstalujte konsole, aby bylo možno "
"používat terminál."

#: kateconsole.cpp:376
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Opravdu si přejete poslat text rourou na konzoli? Tímto budou jakékoliv v "
"textu obsažené příkazy vykonány s vašimi uživatelskými oprávněními."

#: kateconsole.cpp:377
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Poslat rourou na terminál?"

#: kateconsole.cpp:378
#, kde-format
msgid "Pipe to Terminal"
msgstr "Roura na terminál"

#: kateconsole.cpp:406
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Bohužel, nelze přejít do '%1'"

#: kateconsole.cpp:442
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Není lokální soubor: '%1'"

#: kateconsole.cpp:475
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Opravdu si přejete poslat text rourou na Konzoli?\n"
"Tímto budou jakékoliv v textu obsažené příkazy \n"
"vykonány s vašimi uživatelskými oprávněními:\n"
"'%1'"

#: kateconsole.cpp:482
#, kde-format
msgid "Run in Terminal?"
msgstr "Spustit v terminálu?"

#: kateconsole.cpp:483
#, kde-format
msgid "Run"
msgstr "Spustit"

#: kateconsole.cpp:498
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "Sk&rýt panel terminálu"

#: kateconsole.cpp:509
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Zaměřit se mimo panel terminálu"

#: kateconsole.cpp:510 kateconsole.cpp:511
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Přepnout na panel terminálu"

#: kateconsole.cpp:585
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"&Automaticky synchronizovat terminál se současným dokumentem (je-li to možné)"

#: kateconsole.cpp:589 kateconsole.cpp:610
#, kde-format
msgid "Run in terminal"
msgstr "Spustit v terminálu"

#: kateconsole.cpp:591
#, kde-format
msgid "&Remove extension"
msgstr "Odst&ranit příponu"

#: kateconsole.cpp:596
#, kde-format
msgid "Prefix:"
msgstr "Předpona:"

#: kateconsole.cpp:604
#, kde-format
msgid "&Show warning next time"
msgstr "Příště zo&brazit varování"

#: kateconsole.cpp:606
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""

#: kateconsole.cpp:617
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Nastavit proměnnou prostředí &EDITOR na 'kate -b'"

#: kateconsole.cpp:620
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr "Důležité: Dokument musí být zavřen, jinak konzole nemůže pokračovat"

#: kateconsole.cpp:623
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Skrýt Konsole při stisku 'Esc'"

#: kateconsole.cpp:626
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:657
#, kde-format
msgid "Terminal Settings"
msgstr "Nastavení terminálu"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Nástroje"
